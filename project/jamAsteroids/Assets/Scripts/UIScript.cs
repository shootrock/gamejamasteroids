﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {

    public Text livesText;
    public GameObject livesPic;
    public Text scoreText;
    public GameManagerScript gManager;

    public GameObject gameOverObject;

    float livesPicXBegin = 0.0f;
    float livesPicXSeparator = 30.0f;

    float livesPicYAlign = 5.0f;

    int picLives = 0;

    GameObject[] livesCol;

	// Use this for initialization
	void Start () {
        if (gManager == null)
            gManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>();

        livesCol = new GameObject[gManager.lives];

        for (int i = 0; i < gManager.lives; i++)
        {
            GameObject go = Instantiate(livesPic) as GameObject;
            Vector3 newObjectPos = livesText.transform.position;
            newObjectPos.y += livesPicYAlign;
            newObjectPos.x += livesPicXBegin + (i * livesPicXSeparator);
            go.transform.position = newObjectPos;
            go.transform.SetParent(livesText.transform);
            picLives++;
            livesCol[i] = go;
        }
	}
	
	// Update is called once per frame
	void Update () {
        while (picLives >= gManager.lives && picLives > 0)
        {
            Destroy(livesCol[picLives - 1]);
            picLives--;
        }
        scoreText.text = "Score: " + gManager.score;

        if (gManager.gameOver)
        {
            gameOverObject.SetActive(true);
        }
	}
}
