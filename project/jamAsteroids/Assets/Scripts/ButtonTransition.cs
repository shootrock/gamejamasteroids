﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class ButtonTransition : MonoBehaviour {


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void LoadScene(string sceneName) { 
		SceneManager.LoadScene (sceneName); 

	}

    public void PlayButtonClick()
    {
        LoadScene("gameScene");
    }
    public void QuitButtonClick()
    {
        Application.Quit();
    }

    public void MenuButtonClick()
    {
        LoadScene("menuScene");
    }


}
