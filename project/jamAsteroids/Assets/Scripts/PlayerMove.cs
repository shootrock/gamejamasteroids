﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public float moveSpeed = 10.0f;
    public float rotateSpeed = 120.0f;

    public GameObject sceneUp, sceneDown, sceneLeft, sceneRight;

    Vector3 startPos;

    public GameObject bulletObject;

    public GameManagerScript gManager;

    public int scatterShots = 5;

    public float sShotBulletRotate = 20.0f;

    Vector3 rotateOffset;

    bool screenWrapped;

    public int chargedTimer = 120;
    int curTimer = 0;
    bool hasCharged = false;
    public GameObject chargeBullet;

	// Use this for initialization
	void Start () {
        if (gManager == null)
            gManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>();

        startPos = this.gameObject.transform.position;

        rotateOffset = this.transform.eulerAngles;
	}

    private void Update()
    {
        if (Input.GetKey(KeyCode.X))
        {
            curTimer++;
            if (curTimer >= chargedTimer && !hasCharged)
            {
                hasCharged = true;
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (gManager.gameOver == false)
        {
            float forwardInput = Input.GetAxis("Vertical");
            float rotateInput = Input.GetAxis("Horizontal");

            float moveRate = moveSpeed * forwardInput * Time.deltaTime;
            float rotateRate = rotateSpeed * rotateInput * Time.deltaTime;

            this.transform.Rotate(Vector3.down * rotateRate);

            Vector3 newPos = this.transform.position + transform.forward * moveRate;

            //this.transform.rotation = Quaternion.Euler(new Vector3(this.transform.eulerAngles.x, 0, 0));
            //if (this.transform.rotation.eulerAngles.x >= -90 && this.transform.rotation.eulerAngles.x < 90)
            //    this.transform.rotation = Quaternion.Euler(new Vector3(this.transform.eulerAngles.x, 180, 0)); //y and z rotations
            //else
            //    this.transform.rotation = Quaternion.Euler(new Vector3(this.transform.eulerAngles.x, 180, 0));
            transform.position = new Vector3(newPos.x, newPos.y, 0); //fix z axis

            if (Input.GetKeyDown(KeyCode.Space))
            {
                //shoot bullet
                GameObject bullet = Instantiate(bulletObject) as GameObject;

                bullet.transform.position = this.transform.position + (this.transform.forward * 1.0f);
                bullet.transform.rotation = this.transform.rotation;
                bullet.transform.Rotate(-rotateOffset); //offset the initial game object rotation
            }
            if (Input.GetKeyUp(KeyCode.X) && hasCharged)
            {
                hasCharged = false;
                curTimer = 0; //reset charge and timer
                GameObject bullet = Instantiate(chargeBullet) as GameObject;

                bullet.transform.position = this.transform.position + (this.transform.forward * 1.0f);
                bullet.transform.rotation = this.transform.rotation;
                bullet.transform.Rotate(-rotateOffset); //offset the initial game object rotation
            }
            if (Input.GetKeyDown(KeyCode.Z))
            {
                //shoot three bullets
                int halfShots = scatterShots / 2;
                Vector3 initRotate = new Vector3(0, 0, -(sShotBulletRotate * halfShots));
                for (int i = 0; i < scatterShots; i++)
                {
                    GameObject bullet = Instantiate(bulletObject) as GameObject;
                    bullet.transform.rotation = this.transform.rotation;
                    bullet.transform.Rotate(initRotate - rotateOffset + new Vector3(0, 0, i * sShotBulletRotate));
                    bullet.transform.position = this.transform.position + (bullet.transform.forward * 1.0f);
                }
            }

        }

	}

    private void OnTriggerEnter(Collider other)
    {
        if (!screenWrapped)
        {
            if (other.gameObject.tag == "SceneUp")
            {
                Vector3 localPos = this.transform.position;
                localPos.z = 0;
                localPos.y = sceneDown.transform.position.y + 1.5f;
                this.transform.position = localPos;
                screenWrapped = true;
            }
            else if (other.gameObject.tag == "SceneDown")
            {
                Vector3 localPos = this.transform.position;
                localPos.z = 0;
                localPos.y = sceneUp.transform.position.y - 1.5f;
                this.transform.position = localPos;
                screenWrapped = true;
            }
            else if (other.gameObject.tag == "SceneLeft")
            {
                Vector3 localPos = this.transform.position;
                localPos.z = 0;
                localPos.x = sceneRight.transform.position.x - 1.5f;
                this.transform.position = localPos;
                screenWrapped = true;
            }
            else if (other.gameObject.tag == "SceneRight")
            {
                Vector3 localPos = this.transform.position;
                localPos.z = 0;
                localPos.x = sceneLeft.transform.position.x + 1.5f;
                this.transform.position = localPos;
                screenWrapped = true;
            } //screen wrap code
        }

        if (other.gameObject.tag == "Cleanup")
        {
            this.transform.position = startPos;
        }

        if (other.gameObject.tag == "Asteroid")
        {
            if (gManager.lives > 0)
            {
                gManager.lives -= 1;
            }

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "SceneUp" ||
            other.gameObject.tag == "SceneDown" ||
            other.gameObject.tag == "SceneLeft" ||
            other.gameObject.tag == "SceneRight")
        {
            if (screenWrapped)
                screenWrapped = false;
        }
    }
}
