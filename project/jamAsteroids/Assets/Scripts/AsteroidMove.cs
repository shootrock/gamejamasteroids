﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMove : MonoBehaviour {

    public Vector3 rotation;
    public float moveSpeedBig = 5.0f;
    public float moveSpeedSmall = 8.0f;
    public bool isSmall;

    public GameObject smallAsteroid;

    public GameManagerScript gManager;

    public AudioSource asteroidExplosion;

	// Use this for initialization
	void Start () {
        if (gManager == null)
            gManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 newPos;
        if (!isSmall)
        {
            newPos = this.transform.position + (transform.up * moveSpeedBig * Time.deltaTime);
        }
        else
            newPos = this.transform.position + (transform.up * moveSpeedSmall * Time.deltaTime);

        transform.position = new Vector3(newPos.x, newPos.y, 0); //fix z axis
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "SceneUp" ||
            other.gameObject.tag == "SceneDown" ||
            other.gameObject.tag == "SceneLeft" ||
            other.gameObject.tag == "SceneRight" ||
            other.gameObject.tag == "Cleanup")
        {
            Destroy(this.gameObject);
        } //clean up code - don't have scene be flooded by asteroids
    }

     private void OnCollisionEnter(Collision collision)
     {
            if (collision.gameObject.tag == "Bullet")
            {
            gManager.asteroidExplode.Play();
            if (collision.gameObject.GetComponent<BulletMove>().isCharged == false) //if the colliding bullet isn't charged
                    Destroy(collision.gameObject); //destroys the bullet

            if (isSmall)
                {
                    gManager.score += gManager.smallAsteroidScore;
                    Destroy(this.gameObject);
                }
                else
                {
                    GameObject piece1 = Instantiate(smallAsteroid) as GameObject;
                    GameObject piece2 = Instantiate(smallAsteroid) as GameObject;
                    piece1.transform.position = this.transform.position;
                    piece2.transform.position = this.transform.position;
                    piece1.transform.rotation = collision.transform.rotation;
                    piece1.transform.Rotate(0, 0, 90.0f);
                    piece2.transform.rotation = collision.transform.rotation;
                    piece2.transform.Rotate(0, 0, -90.0f);

                    piece1.transform.SetParent(gManager.gameObject.transform);
                    piece2.transform.SetParent(gManager.gameObject.transform); //clean it up, don't have it be flooded with rocks

                    //piece1.transform.position += transform.up * 1.0f;
                    //piece2.transform.position += transform.up * 1.0f;

                    gManager.score += gManager.largeAsteroidScore;
                    Destroy(this.gameObject);
                }

        }
     }

}
