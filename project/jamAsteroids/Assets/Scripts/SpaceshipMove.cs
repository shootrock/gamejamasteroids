﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipMove : MonoBehaviour {

    //give spaceship the asteroid tag - avoid needing to make a new tag for 
    public Vector3 rotation;
    public float moveSpeed = 6.0f;

    public float firingInterval = 60.0f;
    float curTimer = 0.0f;

    public GameManagerScript gManager;

    public GameObject enemyBullet;

    public AudioSource alienShoot;

    // Use this for initialization
    void Start () {
        if (gManager == null)
            gManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 newPos = this.transform.position + (transform.up * moveSpeed * Time.deltaTime);
        transform.position = new Vector3(newPos.x, newPos.y, 0); //fix z axis

        //occasionally firing at the player - may need new bullets - give them the asteroid tag too but using bullet script
        //- stop enemy bullets destroying asteroids as well?
    }

    private void Update()
    {
        if (curTimer >= firingInterval)
        {
            GameObject bullet = Instantiate(enemyBullet) as GameObject;
            bullet.transform.position = this.transform.position;
            bullet.transform.rotation = this.transform.rotation;
            bullet.transform.position += transform.up * 1.0f;
            alienShoot.Play();

            curTimer = 0.0f;
        }
        else
            curTimer++;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "SceneUp" ||
            other.gameObject.tag == "SceneDown" ||
            other.gameObject.tag == "SceneLeft" ||
            other.gameObject.tag == "SceneRight" ||
            other.gameObject.tag == "Cleanup")
        {
            Destroy(this.gameObject);
        } //clean up code - don't have scene be flooded by bullets
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            gManager.score += gManager.shipScore;
            gManager.alienExplode.Play();
            if (collision.gameObject.GetComponent<BulletMove>().isCharged == false) //if the colliding bullet isn't charged
                Destroy(collision.gameObject); //destroys the bullet
            Destroy(this.gameObject);

        }
    }
}
