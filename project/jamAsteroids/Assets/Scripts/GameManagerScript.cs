﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour {

    //curDimension of Screen (x, y) = (-12->12, -5.5->7)
    //governs asteroid/alien ship spawning - based on time? random?
    public float maxAsteroidTimer = 50.0f; //timer until next spawn
    float randAstTimer = 0.0f;
    float curAstTimer = 0;

    //probably a second timer for the ship
    public float maxUFOTimer = 300.0f;
    float randUFOTimer = 0.0f;
    float curUFOTimer = 0;

    public int score;

    public int lives = 4;

    public int shipScore = 200;
    public int largeAsteroidScore = 10;
    public int smallAsteroidScore = 100;

    public GameObject asteroidObject;

    public GameObject ufoObject;

    public GameObject sceneUp, sceneDown, sceneLeft, sceneRight;

    public AudioSource alienExplode;
    public AudioSource asteroidExplode;

    public bool gameOver;
    // Use this for initialization
    void Start()
    {
        randAstTimer = Random.Range(1.0f, maxAsteroidTimer);
        gameOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        //generate asteroids when timer is up
        if (curAstTimer >= randAstTimer)
        {
            GameObject go = Instantiate(asteroidObject) as GameObject;
            //these are generate randomly in the scene
            //perhaps a better form should be generating from random edges
            go.transform.position = new Vector3
                (Random.Range(sceneLeft.transform.position.x + 1.5f, sceneRight.transform.position.x - 1.5f),
                Random.Range(sceneDown.transform.position.y + 1.5f, sceneUp.transform.position.y - 1.5f), 0);

            float DistToEdgeUp = sceneUp.transform.position.y - go.transform.position.y;
            float DistToEdgeDown = go.transform.position.y - sceneDown.transform.position.y;
            float DistToEdgeRight = sceneRight.transform.position.x - go.transform.position.x;
            float DistToEdgeLeft = go.transform.position.x - sceneLeft.transform.position.x;
            //teleport to closest edge
            if (DistToEdgeUp > DistToEdgeDown &&
                DistToEdgeUp > DistToEdgeLeft &&
                DistToEdgeUp > DistToEdgeRight)
            {
                Vector3 newPos = new Vector3(this.transform.position.x, sceneUp.transform.position.y - 1.5f, 0);
                go.transform.position = newPos;
            }
            else if (DistToEdgeDown > DistToEdgeUp &&
                DistToEdgeDown > DistToEdgeLeft &&
                DistToEdgeDown > DistToEdgeRight)
            {
                Vector3 newPos = new Vector3(this.transform.position.x, sceneDown.transform.position.y + 1.5f, 0);
                go.transform.position = newPos;
            }
            else if (DistToEdgeRight > DistToEdgeUp &&
                DistToEdgeRight > DistToEdgeDown &&
                DistToEdgeRight > DistToEdgeLeft)
            {
                Vector3 newPos = new Vector3(sceneRight.transform.position.x - 1.5f, this.transform.position.y, 0);
                go.transform.position = newPos;
            }
            else if (DistToEdgeLeft > DistToEdgeUp &&
                DistToEdgeLeft > DistToEdgeDown &&
                DistToEdgeLeft > DistToEdgeRight)
            {
                Vector3 newPos = new Vector3(sceneLeft.transform.position.x + 1.5f, this.transform.position.y, 0);
                go.transform.position = newPos;
            }

            go.transform.Rotate(0.0f, 0.0f, Random.Range(0.0f, 360.0f));
            go.transform.SetParent(this.gameObject.transform);

            curAstTimer = 0.0f;
            randAstTimer = Random.Range(1.0f, maxAsteroidTimer);
        }
        else
        {
            curAstTimer++;
        } //may also need some clean up code delete asteroids after a certain time?

        //generate UFO's when timer is up
        if (curUFOTimer >= randUFOTimer)
        {
            GameObject go = Instantiate(ufoObject) as GameObject;
            //these are generate randomly in the scene
            //perhaps a better form should be generating from random edges
            go.transform.position = new Vector3
                (Random.Range(sceneLeft.transform.position.x + 1.5f, sceneRight.transform.position.x - 1.5f), 
                Random.Range(sceneDown.transform.position.y + 1.5f, sceneUp.transform.position.y - 1.5f), 0);

            float DistToEdgeUp = sceneUp.transform.position.y - go.transform.position.y;
            float DistToEdgeDown = go.transform.position.y - sceneDown.transform.position.y;
            float DistToEdgeRight = sceneRight.transform.position.x - go.transform.position.x;
            float DistToEdgeLeft = go.transform.position.x - sceneLeft.transform.position.x;
            //teleport to closest edge
            if (DistToEdgeUp > DistToEdgeDown &&
                DistToEdgeUp > DistToEdgeLeft &&
                DistToEdgeUp > DistToEdgeRight)
            {
                Vector3 newPos = new Vector3(this.transform.position.x, sceneUp.transform.position.y - 1.5f, 0);
                go.transform.position = newPos;
            }
            else if (DistToEdgeDown > DistToEdgeUp &&
                DistToEdgeDown > DistToEdgeLeft &&
                DistToEdgeDown > DistToEdgeRight)
            {
                Vector3 newPos = new Vector3(this.transform.position.x, sceneDown.transform.position.y + 1.5f, 0);
                go.transform.position = newPos;
            }
            else if (DistToEdgeRight > DistToEdgeUp &&
                DistToEdgeRight > DistToEdgeDown &&
                DistToEdgeRight > DistToEdgeLeft)
            {
                Vector3 newPos = new Vector3(sceneRight.transform.position.x - 1.5f, this.transform.position.y, 0);
                go.transform.position = newPos;
            }
            else if (DistToEdgeLeft > DistToEdgeUp &&
                DistToEdgeLeft > DistToEdgeDown &&
                DistToEdgeLeft > DistToEdgeRight)
            {
                Vector3 newPos = new Vector3(sceneLeft.transform.position.x + 1.5f, this.transform.position.y, 0);
                go.transform.position = newPos;
            }

            go.transform.Rotate(0.0f, 0.0f, Random.Range(0.0f, 360.0f));
            go.transform.SetParent(this.gameObject.transform);

            curUFOTimer = 0.0f;
            randUFOTimer = Random.Range(1.0f, maxUFOTimer);
        }
        else
        {
            curUFOTimer++;
        } //may also need some clean up code delete asteroids after a certain time?
        if (lives <= 0)
        {
            gameOver = true;
            SceneManager.LoadScene("gameOverScene");
        }
    }
}
