﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

    public Vector3 rotation;
    public float moveSpeed = 20.0f;

    public bool isCharged;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 newPos = this.transform.position + (transform.up * moveSpeed * Time.deltaTime);
        transform.position = new Vector3(newPos.x, newPos.y, 0); //fix z axis
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "SceneUp" ||
            other.gameObject.tag == "SceneDown" ||
            other.gameObject.tag == "SceneLeft" ||
            other.gameObject.tag == "SceneRight" ||
            other.gameObject.tag == "Cleanup")
        {
            Destroy(this.gameObject);
        } //clean up code - don't have scene be flooded by bullets
    }

}
